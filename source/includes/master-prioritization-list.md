1. <a href="/handbook/engineering/security/#severity-and-priority-labels-on-security-issues">Security fixes</a>
1. Data-loss prevention
1. Availability
1. Fixing regressions (things that worked before)
1. Velocity of new features, user experience improvements, technical debt, community contributions, and all other improvements
1. Lastly, behaviors that yield higher predictability (because this inevitably slows us down)
